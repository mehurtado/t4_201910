package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.OrderedHeap;
import model.data_structures.UnorderedHeap;
import model.value_objects.Compania;
import model.value_objects.Servicio;
import model.value_objects.Taxi;

public class Project4Manager {

	public  final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public  final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public  final String DIRECCION_LARGE_JSON = "large";
	public  final String DIRECCION_LARGE= "./data/Large/taxi-trips-wrvz-psew-subset-0";
	public  final String COMPLEMENT_DIR = "-02-2017.json";
	public  final String DIRECCION_TEST_JSON = "./data/Test.json";
	public  final int CANTIDAD_ARCHIVOS = 7;

	private OrderedHeap<Servicio> srvcss;
	private OrderedHeap<Taxi> taxis;

	public void loadTaxis(String ruta) {
		srvcss = new OrderedHeap<>(2000);
		taxis = new OrderedHeap<>(2000);
		Gson g = new GsonBuilder().create();
		if(!ruta.equals(DIRECCION_LARGE_JSON)) cargaSML(g, ruta);
		else for(int i = 2; i<CANTIDAD_ARCHIVOS+2;i++) cargaSML(g, DIRECCION_LARGE + i + COMPLEMENT_DIR);}

	private void cargaSML(Gson g, String dirJson){
		try{FileInputStream stream = new FileInputStream(new File(dirJson));
		JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8")); reader.beginArray();
		while (reader.hasNext()) {
			Servicio srvc = g.fromJson(reader, Servicio.class); 
			Taxi t = new Taxi(srvc.getTaxiId(), srvc.getCompany());
			Taxi T = taxis.get(t);
			if(T == null) 
				taxis.insert(t);
			else T.addService(srvc);
			srvcss.insert(srvc);
		}
		System.out.println(srvcss.size());
		System.out.println(taxis.size());
		reader.close();
		}catch(Exception e){System.out.println("Error en la carga del los archivos");e.printStackTrace();}}

	public OrderedHeap<Taxi> darTaxis() {
		OrderedHeap<Taxi> temp = new OrderedHeap<>(2000);
		OrderedHeap<Taxi> tempp = taxis;
		while(tempp.size() > 0) {
			Taxi t = tempp.remove();
			temp.insert(t);
		}
		return temp;
	}

	public OrderedHeap<Compania> darCompanias(){
		OrderedHeap<Compania> temp = new OrderedHeap<>(1000);
		OrderedHeap<Taxi> tempp = taxis;

		while(tempp.size() > 0) {
			Taxi t = tempp.remove();
			Compania c = new Compania(t.getCompany());
			Compania C = temp.get(c);
			if(C == null) 
				temp.insert(c);
			else C.addTaxi(t);
		}
		return temp;
	}

}
