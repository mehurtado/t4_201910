package model.value_objects;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	
	private String trip_id;

	private String taxi_id;

	private String company;

	//	______________________

	private String trip_start_timestamp;

	private String trip_end_timestamp;

	private int dropoff_community_area;

	private int pickup_community_area;

	private double trip_miles;

	private double trip_total;

	private int trip_seconds;

	public void setId(String id){
		this.trip_id = id;
	}

	public String getPickArea(){
		return Integer.toString(pickup_community_area);
	}
	public String  getDropArea(){
		return Integer.toString(dropoff_community_area);
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId(){return trip_id;}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return trip_seconds;}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return trip_miles;}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return trip_total;}

	public String getTripStart() {
		return trip_start_timestamp;}

	public String getTripEnd() {
		return trip_end_timestamp;}

	public String getCompany(){
		return company;
	}
	public boolean hasCompany() {
		return (this.company != null && this.company != "");
	}

	public boolean estaEnRango(RangoFechaHora rango){
		boolean esta1 = false;
		boolean esta2 = false;
		String fechaIn = rango.getFechaInicial();
		String fechaFn = rango.getFechaFinal();
		if(trip_start_timestamp != null && trip_start_timestamp.compareTo(fechaIn) >= 0)
			esta1 = true;
		if(trip_end_timestamp != null && (trip_end_timestamp.compareTo(fechaFn)<=0))esta2 = true;
		return (esta1 && esta2);}

	@Override
	public int compareTo(Servicio arg0) {
		return (this.trip_id.compareTo(arg0.getTripId())<0)?-1:
			(this.trip_id.compareTo(arg0.getTripId())>0)?1:0;}
}
