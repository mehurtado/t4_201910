package model.value_objects;

import model.data_structures.DoubleLinkedList;

public class Compania implements Comparable<Compania> {

	private String nombre;

	private DoubleLinkedList<Taxi> taxisInscritos;	

	public Compania(String nombre){
		this.nombre = nombre;
		taxisInscritos = new DoubleLinkedList<>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DoubleLinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public int darTotalServicios() {
		int cant = 0;
		while(taxisInscritos.hasNext()) {
			Taxi t = taxisInscritos.getCurrent();
			cant += t.getCantOfServices();
			taxisInscritos.next();
		}
		return cant;
	}

	@Override
	public int compareTo(Compania o) {
		return (this.nombre != null)?this.nombre.compareTo(o.getNombre()):0;
	}

	public void addTaxi(Taxi t) {
		this.taxisInscritos.add(t);
	}
}
