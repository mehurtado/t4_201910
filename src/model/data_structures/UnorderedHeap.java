package model.data_structures;

import Api.IHeapArray;

/**
 * 
 * @author Anderson Barrag�n Agudelo
 *
 * @param <T>
 */
public class UnorderedHeap<T extends Comparable<T>> implements IHeapArray<T>{

	private T[] data;
	private int size;

	@SuppressWarnings("unchecked")
	public UnorderedHeap(int capacity) {data = (T[]) new Comparable[capacity];}

	@Override
	public void insert(T element) {
		if(size == data.length) 
			expand();
		data[size++] = element;
	}

	@Override
	public T remove() {
		int max = 0;
		for (int i = 0; i < data.length; i++)
			if (less(max, i)) max = i;
		exchange(max, size-1);
		T inf = data[size - 1];
		data[--size] = null;
		return inf;
	}

	@Override
	public int size() {return size;}

	/**
	 * 	Take the array and make a new array whit size + 1/10(size)
	 * @param Array array to expand
	 * @return new array, with all elements of the "Array"
	 */
	@SuppressWarnings("unchecked")
	private void expand() {
		T[] newArray = (T[]) new Comparable[data.length + 1000];
		for (int i = 0; i < data.length; i++) {
			newArray[i] = data[i];
		}
		data = newArray;
	}

	@Override
	public boolean less(int i, int j) {
		return (data[i]!= null && data[j] != null)? data[i].compareTo(data[j]) < 0:false;
	}

	@Override
	public void exchange(int i, int j) {
		T t = data[i];
		data[i] = data[j];
		data[j] = t;
	}

	public T get(T element) {
		if(size > 0)
			for (T t : data) 
				if(t.compareTo(element) == 0) return t;
		return null;
	}

	public String toString() {
		String ret = "";
		for (T t : data) 
			ret += t + " ";
		return ret;
	}

}
