package model.data_structures;
public class DoubleLinkedList<T extends Comparable<T>>{

	private NodeDL<T> root;

	private NodeDL<T> current;

	private NodeDL<T> head;

	private int size;

	public DoubleLinkedList() {
		root = current = head = null; size = 0;
	}

	public boolean hasNext(){
		return current != null;}

	public boolean isEmpty(){
		return(size() == 0);}

	public T remove(T element){
		T toRet = (root != null)?root.get(element):null;
		if(toRet != null)delete(element);
		current = root;
		return toRet;
	}

	public boolean add(T element) {
		boolean added = false;
		size++;
		NodeDL<T> newNode = new NodeDL<T>(element);
		if(root == null){
			root = newNode;
			current = root;
			head = root;
			added = true;
		}
		else{
			head.setNext(newNode);
			newNode.setPrev(head);
			head = newNode;
			added = true;
		}
		return added;
	}

	public boolean addFirst(T element) {
		boolean added = false;
		NodeDL<T> newNode = new NodeDL<T>(element);
		size++;
		if(root == null){
			root = newNode;
			current = root;
			head = root;
			added = true;
		}
		else{
			newNode.setNext(root);
			root.setPrev(newNode);
			root = newNode;
			added = true;
		}
		return added;
	}


	public boolean delete(T element) {
		boolean deleted = false;
		NodeDL<T> n = getNode(element);
		if(root != null && root != head && n != null){
			if(root.getItem().compareTo(element) == 0){
				root = root.next();
				root.setPrev(null);
				deleted = true;
			}else if(head.getItem().compareTo(element) == 0){
				head = head.previous();
				head.setNext(null);
				deleted = true;
			}else {
				n.setItem(null);
				n.previous().setNext(n.next());
				n.next().setPrev(n.previous());
				deleted = true;
			}
		}
		return deleted;
	}

	public NodeDL<T> getNode(T element) {
		return (root != null)? root.getNode(element):null;
	}


	//	public T get(T element) {
	//		return (root != null)? root.get(element):null;
	//	}


	public int size() {
		return size;
	}


	public NodeDL<T> get(int pos) {
		return (root != null)? root.get(pos):null;
	}


	public T getCurrent() {return current.getItem();}


	public T next() {
		T r = null;
		if(current != null)r = current.getItem(); current = current.next();
		return r;
	}

	@SuppressWarnings("unchecked")
	public void shellSort(){
		T[] arreglo = (T[]) (new Comparable[size]);
		int contador = 0;
		while(hasNext()){
			T a = current.getItem();
			arreglo[contador] = a;
			contador ++;
			next();
		}
		root = head = null;
		if(arreglo.length > 0){
			shellSort(arreglo);
			for (T t : arreglo) {
				add(t);
			}
		}
	}

	private void shellSort(T[] arreglo){
		int N = arreglo.length; int h = 1;
		while (h < N/3)
			h = 3*h + 1; 
		while (h >= 1) { for (int i = h; i < N; i++)
			for (int j = i; j >= h && less(arreglo[j], arreglo[j-h]); j -= h)
				exch(arreglo, j, j-h); } h = h/3;
	}

	private void exch(T[] arreglo, int i, int j) {
		T t = (T)arreglo[i];
		arreglo[i] = arreglo[j];
		arreglo[j] = t; 
	}

	private boolean less(T object1, T object2){
		return (object1).compareTo(object2) < 0; }
}
