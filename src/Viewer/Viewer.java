package Viewer;

import java.util.Scanner;

import model.data_structures.OrderedHeap;
import model.data_structures.UnorderedHeap;
import model.logic.Project4Manager;
import model.value_objects.Compania;
import model.value_objects.Taxi;

public class Viewer {
	private static Timer t = new Timer();
	private static Project4Manager manager;

	public static void main(String[] args) {
		manager = new Project4Manager();

		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while(!fin) {
			printMenu();
			int opcion = sc.nextInt();
			switch (opcion) {
			case 1:
				t.start();
				manager.loadTaxis(manager.DIRECCION_SMALL_JSON);
				t.end();
				break;
			case 2:
				t.start();
				OrderedHeap<Taxi> req = manager.darTaxis();
				while(req.size() > 0) {
					Taxi t = req.remove();
					System.out.println("El taxi con identificador: "+t.getTaxiId());
					System.out.println("	Hizo: "+t.getCantOfServices()+ " Servicios");
				}
				t.end();
				break;
			case 3:
				t.start();
				OrderedHeap<Compania> req2 = manager.darCompanias();
				while(req2.size() > 0) {
					Compania c = req2.remove();
					System.out.println("La compania: "+ c.getNombre());
					System.out.println("	Tiene: "+ c.getTaxisInscritos().size()+ " Taxis");
					System.out.println("	Los cuales realizaron en total: " + c.darTotalServicios() +" servicios");
				}
				t.end();
				break;
			case 4: fin = true; sc.close(); break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("1. Cargar en el arreglo");
		System.out.println("2. Req 1 Hecho por anderson Barrag�n");
		System.out.println("3. Req 2 Hecho por anderson Barrag�n");
		System.out.println("4. Salir");
	}
}
